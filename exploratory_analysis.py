import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

clinvar = pd.read_csv('clinvar_conflicting.csv')

miss_data = sns.heatmap(clinvar.isnull(),yticklabels=False, xticklabels=True,cbar=False,cmap='viridis')
plt.tight_layout()
fig = miss_data.get_figure()
fig.savefig('Missing Data Chart')

'''
Eliminate Irrelevant or Damaged data columns

Columns:
    CLNDISDBINCL
    CLNDNINCL
    CLNSIGINCL
    CLNVI
    MC
    SRR
    
'''

